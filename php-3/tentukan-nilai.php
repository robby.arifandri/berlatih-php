<?php
function tentukan_nilai($number)
{
    //  kode disini
    if($number > 100 || $number <0) {
        echo "Input di luar rentang yang seharusnya (0-100) <br>";
    }
    else if($number >= 85) {
        echo "Sangat Baik <br>";
    }
    else if($number >= 70) {
        echo "Baik <br>";
    }
    else if($number >= 60) {
        echo "Cukup <br>";
    }
    else {
        echo "Kurang <br>";
    }
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>