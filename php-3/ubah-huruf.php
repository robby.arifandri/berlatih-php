<?php
function ubah_huruf($string){
//kode di sini
    $alphabet = "abcdefghijklmnopqrstuvwxyza"; //TODO add 'a' after z
    $output = "";
    for ($i=0; $i<strlen($string); $i++) {
        $pos = strpos($alphabet, $string[$i]);
        $output .= substr($alphabet, $pos+1, 1);
    }
    echo $output."<br>";
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>